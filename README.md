# Script

Script for managing mobile network for Android devices

the script monitors Internet availability by disabling the GSM network if the Internet is available via wi-fi

for example, to save battery power...

# Configure
1 [root][root] your device

2 install [busybox][busybox] or [busyboxinstaller][busyboxinstaller]

3 run in adb shell or termux shell as root user
``` bash
CRONTAB="/data/local/crontab" && \
SBIN="/data/local/sbin" && \
mkdir -p $CRONTAB $SBIN && \
curl "https://gitlab.com/dedmin/android-wifi-mode/-/raw/master/wifi_mode.sh" -o $SBIN/wifi_mode.sh && \
echo "crond -b -c $CRONTAB" > /data/adb/service.d/crond && \
chmod +x /data/adb/service.d/crond $SBIN/wifi_mode.sh && \
echo "* * * * * sh $SBIN/wifi_mode.sh" > $CRONTAB/root && \
settings put global airplane_mode_radios cell,bluetooth,nfc,wimax
```

[root]: https://github.com/topjohnwu/Magisk
[busybox]: https://f-droid.org/ru/packages/ru.meefik.busybox
[busyboxinstaller]:https://f-droid.org/ru/packages/com.smartpack.busyboxinstaller