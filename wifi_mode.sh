#!/bin/sh

# Author: https://gitlab.com/dedmin 
# Email dmrepo@proton.me
# Version: 03-08-2024
# License: GPLv3

WIFI=wlan0
CHECK="dns.quad9.net:5053"
STATUS=$(settings get global airplane_mode_on)

if ip link show $WIFI | grep "state UP" > /dev/null 2>&1; then
  if curl --interface $WIFI -s $CHECK -m 10 -o /dev/null; then
    if [ "$STATUS" -eq 0 ]; then
      settings put global airplane_mode_on 1
      am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true
    fi
  fi
else
  if [ "$STATUS" -eq 1 ]; then
    settings put global airplane_mode_on 0
    am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false
  fi
fi